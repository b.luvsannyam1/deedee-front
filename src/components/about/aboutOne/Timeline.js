import React, { useContext } from "react";
import { ContentData } from "../../context";

const Timeline = () => {
  const { data, Locale } = useContext(ContentData);
  let counter = 0;
  return (
    <section className="pos-r o-hidden">
      <canvas id="canvas"></canvas>
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-md-12">
            {data.content.timeline.time.map((item) => (
              <div className="timeline-wrap">
                <ul className="timeline timeline--first list-unstyled">
                  <li className="timeline-title">
                    <h2>{item.year}</h2>
                  </li>
                  {item.events[`${Locale}`].map((timeline, index) => (
                    <li
                      className={`timeline-inner ${
                        counter++ % 2 === 0 ? "timeline-left" : "timeline-right"
                      }`}
                      key={index}
                    >
                      <div className="timeline-content">
                        <h3>{timeline.title}</h3>
                        <p>{timeline.description}</p>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Timeline;
