import React, { useState, useEffect, useContext } from "react";
import { ContentData } from "../../context";
const Blog = () => {
  const { Locale } = useContext(ContentData);
  const [data, setData] = useState(null);
  const fetchData = () => {
    var myHeaders = new Headers();

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(`http://localhost:8080/server/v1/blog/featured`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        setData(result.data[0]);
      })
      .catch((error) => console.log("error", error));
  };
  useEffect(() => {
    if (data === null) fetchData();
  });
  return (
    <>
      {data === null ? (
        ""
      ) : (
        <section>
          <div className="container">
            <div className="row text-center">
              <div className="col-lg-8 col-md-12 ml-auto mr-auto">
                <div className="section-title">
                  <div className="title-effect">
                    <div className="bar bar-top"></div>
                    <div className="bar bar-right"></div>
                    <div className="bar bar-bottom"></div>
                    <div className="bar bar-left"></div>
                  </div>
                  <h6>{data.content[Locale].title}</h6>
                  <p>{data.content[Locale].description}</p>
                </div>
              </div>
            </div>
            <div className="row">
              {Object.keys(data.details).map(function (key, index) {
                return (
                  <div className="col-lg-4 col-md-12 md-mt-5" key={index}>
                    <div className="post">
                      <div className="post-image">
                        <img
                          className="img-fluid h-100 w-100"
                          src={`${data.details[key].img_src}`}
                          alt=""
                        />
                        <a className="post-categories" href="/#">
                          Photo
                        </a>
                      </div>
                      <div className="post-desc">
                        <div className="post-title">
                          <h4>
                            <a href={`/blog/${data.details[key]._id}`}>
                              {data.details[key].content[Locale].title}
                            </a>
                          </h4>
                        </div>
                        <p>{data.details[key].content[Locale].description}</p>
                        <a className="icon-btn mt-4" href="/blogsingle">
                          {" "}
                          <i className="la la-angle-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                );
              })}
              {/* {data.details.map((post, index) => (
                <div className="col-lg-4 col-md-12 md-mt-5" key={index}>
                  {index}
                  <div className="post">
                    <div className="post-image">
                      <img
                        className="img-fluid h-100 w-100"
                        src={`${URL.img_url}${post["img_src"]}`}
                        alt=""
                      />
                      <a className="post-categories" href="/#">
                        Photo
                      </a>
                    </div>
                    <div className="post-desc">
                      <div className="post-meta">
                        <ul className="list-inline">
                          <li>
                            <i className="la la-calendar mr-1"></i>{" "}
                            {post["date"]}
                          </li>
                          <li>
                            <i className="la la-user mr-1"></i> {post["author"]}
                          </li>
                        </ul>
                      </div>
                      <div className="post-title">
                        <h4>
                          <a href="/blogsingle">{post["title"]}</a>
                        </h4>
                      </div>
                      <p>{post["description"]}</p>
                      <a className="icon-btn mt-4" href="/blogsingle">
                        {" "}
                        <i className="la la-angle-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
              ))} */}
            </div>
          </div>
        </section>
      )}
    </>
  );
};

export default Blog;
