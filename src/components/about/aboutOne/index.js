import React, { useEffect, useContext } from "react";
import Title from "./Title";
import Services from "./Services";
import Timeline from "./Timeline";
import Team from "./Team";
// import Pricing from "../../home/homeOne/Pricing";
import Testimonial from "./Testimonial";

import { ContentData } from "../../context";

const AboutOne = () => {
  const { data } = useContext(ContentData);
  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  });
  return (
    <>
      <Title />
      <div className="page-content">
        <Services data={data.content.services[0].services} />
        <Timeline />
        <Team data={data.content.team[0]} />
        {/* <Pricing data={data.content.pricing} /> */}
        <Testimonial data={data.content.testimonial[0]} />
      </div>
    </>
  );
};
export default AboutOne;
