import React, { useContext, useState } from "react";
import { ContentData } from "./context";
//import ColorCustomizer from "./ColorCustomizer";

import { FormattedMessage } from "react-intl";
import ScrollTop from "./ScrollTop";
import { NavLink } from "react-router-dom";
const Footer1 = () => {
  const { data, Locale } = useContext(ContentData);
  const [Email, setEmail] = useState("");
  const SendEmail = () => {
    console.log(`Sending email ${Email}`);
  };
  return (
    <>
      {/* footer start */}
      <footer className="footer footer-1 pos-r" data-bg-img="images/bg/05.png">
        <div className="subscribe-box">
          <div className="container">
            <div className="row subscribe-inner align-items-center">
              <div className="col-md-6 col-sm-12">
                <h4>
                  {" "}
                  <FormattedMessage
                    id="footer.subscribe"
                    defaultMessage="Манайхаас мэдээлэл авах бол"
                  />
                </h4>
              </div>
              <div className="col-md-6 col-sm-12 sm-mt-2">
                <div className="subscribe-form sm-mt-2">
                  <div id="mc-form" className="group">
                    <input
                      type="email"
                      value={Email}
                      name="EMAIL"
                      className="email"
                      id="mc-email"
                      style={{ border: "1px solid" }}
                      placeholder={
                        Locale === "mn" ? "Цахим шуудан" : "Email Address"
                      }
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <button
                      className="btn btn-theme"
                      type="submit"
                      name="subscribe"
                      value="Subscribe"
                      onClick={SendEmail}
                    >
                      <FormattedMessage
                        id="footer.button"
                        defaultMessage="Дагах"
                      />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="primary-footer">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-6">
                <div className="footer-logo">
                  <img
                    id="footer-logo-img"
                    src={`${data.logo}`}
                    className="img-center"
                    alt=""
                  />
                </div>
                <p className="mb-0">{data.description}</p>
              </div>
              <div className="col-lg-4 col-md-6 pl-md-5 sm-mt-5 footer-list justify-content-between d-flex">
                <ul className="list-unstyled w-100">
                  <li>
                    <NavLink
                      className="nav-link"
                      activeClassName="active"
                      to="/"
                    >
                      <FormattedMessage id="nav.home" defaultMessage="Нүүр" />
                    </NavLink>
                  </li>
                  <li>
                    {" "}
                    <NavLink
                      className="nav-link"
                      activeClassName="active"
                      to="/about-us"
                    >
                      <FormattedMessage
                        id="nav.about-us"
                        defaultMessage="Бидний тухай"
                      />
                    </NavLink>
                  </li>
                </ul>
                <ul className="list-unstyled w-100">
                  <li>
                    {" "}
                    <NavLink
                      className="nav-link"
                      activeClassName="active"
                      to="/blogs"
                    >
                      {" "}
                      <FormattedMessage id="nav.blogs" defaultMessage="Блог" />
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      className="nav-link"
                      activeClassName="active"
                      to="/contact-us"
                    >
                      {" "}
                      <FormattedMessage
                        id="nav.contact-us"
                        defaultMessage="Бидэнтэй холбогдох"
                      />
                    </NavLink>
                  </li>
                </ul>
              </div>
              <div className="col-lg-4 col-md-12 md-mt-5">
                <ul className="media-icon list-unstyled">
                  <li>
                    <p className="mb-0">
                      <FormattedMessage
                        id="nav.address"
                        defaultMessage="Хаяг"
                      />
                      : <b>{data.address}</b>
                    </p>
                  </li>
                  <li>
                    <FormattedMessage
                      id="nav.email"
                      defaultMessage="Цахим шуудан"
                    />
                    :{" "}
                    <a href={`mailto:${data.email}`}>
                      <b>{data.email}</b>
                    </a>
                  </li>
                  <li>
                    <FormattedMessage
                      id="nav.number"
                      defaultMessage="Утасны дугаар"
                    />
                    :{" "}
                    <a href={`tel:+${data.number}`}>
                      <b>+{data.number}</b>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="secondary-footer">
          <div className="container">
            <div className="copyright">
              <div className="row align-items-center">
                <div className="col-lg-6 col-md-12">
                  {" "}
                  <span>
                    <FormattedMessage
                      id="nav.copyright"
                      defaultMessage="Copyright 2021 Дээдээ машин"
                    />
                  </span>
                </div>
                <div className="col-lg-6 col-md-12 text-lg-right md-mt-3">
                  <div className="footer-social">
                    <ul className="list-inline">
                      <li className="mr-2">
                        <a href={`https://facebook/${data.facebook}`}>
                          <i className="fab fa-facebook-f"></i> Facebook
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      {/* footer end */}
      {/* <ColorCustomizer /> */}
      <ScrollTop />
    </>
  );
};

export default Footer1;
