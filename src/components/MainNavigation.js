import React, { useContext } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomeOne from "./home/homeOne";
import About from "./about/aboutOne/";
import Error404 from "./pages/error404";
import Blogs from "./blogs/rightSidebar";
import Footer1 from "./Footer1";
import Login from "./login";
import Blog from "./blogs/blogSingle";
import Contact from "./contact/contactOne";
import Header1 from "./Header1";
import Loader from "./Loader";
import LoginSuccess from "./pages/loginSuccess";

import { ContentData } from "./context";

function MainNavigation() {
  const { data } = useContext(ContentData);
  return (
    <>
      <Router>
        {data ? (
          <>
            <Header1 />
            <Switch>
              <Route path="/" exact={true} component={HomeOne} />
              <Route path="/about-us" exact={true} component={About} />
              <Route path="/blogs/" exact={true} component={Blogs} />
              <Route path="/blogs/:id" exact={true} component={Blogs} />
              <Route path="/blog/:id" exact={true} component={Blog} />
              <Route path="/contact-us" exact={true} component={Contact} />
              <Route
                path="/login-success"
                exact={true}
                component={LoginSuccess}
              />{" "}
              <Route path="/login" exact={true} component={Login} />
              <Route component={Error404} />
            </Switch>
            <Footer1 />
          </>
        ) : (
          <Loader />
        )}
      </Router>
    </>
  );
}

export default MainNavigation;
