import React, { useEffect } from "react";
import Title from "./Title";
import About from "./About";
import PriceTable from "./PriceTable";

const ServiceOne = () => {
  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  });
  return (
    <>
      <Title />
      <div className="page-content">
        <About />
        <PriceTable />
        {/* <Testimonial /> */}
        {/* <Blog /> */}
      </div>
    </>
  );
};
export default ServiceOne;
