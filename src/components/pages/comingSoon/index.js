import React from "react";
import PageContent from "./PageContent";

const ComingSoon = () => {
  return (
    <>
      <PageContent />
    </>
  );
};
export default ComingSoon;
