import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { ContentData } from "./context";
import { FormattedMessage } from "react-intl";
import dataHeader from "./data.json";
import "./theme-color/color-4.css";

const Header1 = () => {
  const { data, Locale, changeLocale } = useContext(ContentData);
  return (
    <>
      <header
        id="site-header"
        className="header header-1"
        style={{ position: "fixed" }}
      >
        <div className="container-fluid">
          <div id="header-wrap" className="box-shadow">
            <div className="row">
              <div className="col-lg-12">
                {/* Navbar */}
                <nav className="navbar navbar-expand-lg">
                  <a className="navbar-brand logo" href="/">
                    <img
                      id="logo-img"
                      className="img-center"
                      src={`${data.logo}`}
                      alt=""
                    />
                  </a>
                  <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNavDropdown"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                  >
                    {" "}
                    <span></span>
                    <span></span>
                    <span></span>
                  </button>
                  <div
                    className="collapse navbar-collapse"
                    id="navbarNavDropdown"
                  >
                    {/* Left nav */}
                    <ul
                      id="main-menu"
                      className="nav navbar-nav ml-auto mr-auto"
                    >
                      <li className="nav-item">
                        {" "}
                        <NavLink
                          className="nav-link"
                          activeClassName="active"
                          to="/"
                        >
                          <FormattedMessage
                            id="nav.home"
                            defaultMessage="Нүүр"
                          />
                        </NavLink>
                      </li>
                      <li className="nav-item"> </li>
                      <li className="nav-item">
                        <NavLink
                          className="nav-link"
                          activeClassName="active"
                          to="/about-us"
                        >
                          <FormattedMessage
                            id="nav.about-us"
                            defaultMessage="Бидний тухай"
                          />
                        </NavLink>
                      </li>

                      <li className="nav-item">
                        {" "}
                        <NavLink
                          className="nav-link"
                          activeClassName="active"
                          to="/blogs"
                        >
                          {" "}
                          <FormattedMessage
                            id="nav.blogs"
                            defaultMessage="Блог"
                          />
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        {" "}
                        <NavLink
                          className="nav-link"
                          activeClassName="active"
                          to="/contact-us"
                        >
                          {" "}
                          <FormattedMessage
                            id="nav.contact-us"
                            defaultMessage="Бидэнтэй холбогдох"
                          />
                        </NavLink>
                      </li>
                    </ul>
                  </div>

                  <button onClick={changeLocale} className="btn btn-locale">
                    {Locale === "mn" ? "ENG" : "MON"}
                  </button>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header1;
