import React, { createContext, useState, useEffect } from "react";
import { IntlProvider } from "react-intl";
import messagesInFrench from "./locale/locale_eng.json";
import messagesInMongolian from "./locale/locale_mn.json";

export const ContentData = createContext();

const messages = {
  en: messagesInFrench,
  mn: messagesInMongolian,
};
export const Context = (props) => {
  const [data, setData] = useState({ mn: null, en: null });

  const [Locale, setLocale] = useState("mn");
  const fetchData = () => {
    var myHeaders = new Headers();

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(`http://localhost:8080/server/v1/home`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        setData({ mn: result.data[0], en: result.data[1] });
      })
      .catch((error) => console.log("error", error));
  };
  useEffect(() => {
    if (data.mn === null) fetchData();
  });
  const changeLocale = () => {
    setLocale(Locale === "mn" ? "en" : "mn");
  };
  return (
    <IntlProvider
      messages={messages[Locale]}
      locale={Locale}
      defaultLocale="mn"
    >
      <ContentData.Provider
        value={{ data: data[Locale], Locale, changeLocale }}
      >
        {props.children}
      </ContentData.Provider>
    </IntlProvider>
  );
};
