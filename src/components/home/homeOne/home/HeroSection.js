import React from "react";

const HeroSectionOne = ({ data }) => {
  return (
    <>
      {/* hero section :: start */}
      <section
        className="fullscreen-banner banner p-0 bg-contain bg-pos-r"
        data-bg-img={"" + data.img_url}
      >
        <div className="spinner-eff">
          <div className="spinner-circle circle-1"></div>
          <div className="spinner-circle circle-2"></div>
        </div>
        <div className="align-center pt-0">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-lg-6 col-md-12 order-lg-1">
                <img
                  className="img-center wow jackInTheBox"
                  data-wow-duration="3s"
                  src={"" + data.img_url}
                  alt=""
                />
              </div>
              <div className="col-lg-6 col-md-12 order-lg-1 md-mt-5">
                <h1 className="mb-4 wow fadeInUp" data-wow-duration="1.5s">
                  {data.title}
                </h1>
                <p
                  className="lead mb-4 wow fadeInUp"
                  data-wow-duration="2s"
                  data-wow-delay="0.3s"
                >
                  {data.description}
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default HeroSectionOne;
