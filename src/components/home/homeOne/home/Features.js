import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

const Features = ({ data }) => {
  return (
    <>
      <section style={{ marginBottom: 150, zIndex: 100 }}>
        <div className="container-fluid">
          <div className="row custom-mt-10 z-index-1 md-mt-0">
            <div className="col-md-12">
              <Carousel
                responsive={responsive}
                infinite={true}
                autoPlaySpeed={2000}
                autoPlay={true}
              >
                {data.details.map((feature, index) => (
                  <div className="item" key={index} style={{ zIndex: 100 }}>
                    <div
                      className="featured-item style-1"
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        alignContent: "center",
                        boxShadow: "0px 0px 14px -4px rgba(55,8,255,0.78)",
                        margin: "10px",
                      }}
                    >
                      <div
                        className="featured-icon"
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignContent: "center",
                        }}
                      >
                        <i className={`${feature["icon"]}`}></i>
                        <span className="rotateme"></span>
                      </div>
                      <div
                        className="featured-title"
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignContent: "center",
                        }}
                      >
                        <h5>{feature["title"]}</h5>
                      </div>
                      <div
                        className="featured-desc"
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignContent: "center",
                        }}
                      >
                        <p>{feature["description"]}</p>
                      </div>
                    </div>
                  </div>
                ))}
              </Carousel>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default Features;
