import React from "react";
import "react-multi-carousel/lib/styles.css";
const Team = ({ data }) => {
  return (
    <>
      <section className="bg-effect right pos-r o-hidden">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 col-md-12">
              <div className="section-title">
                <div className="title-effect">
                  <div className="bar bar-top"></div>
                  <div className="bar bar-right"></div>
                  <div className="bar bar-bottom"></div>
                  <div className="bar bar-left"></div>
                </div>
                <h6>{data.title}</h6>
                <h2 className="title">{data.sub_title}</h2>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid p-0">
          <div className="row">
            <div className="col-xl-10 col-lg-11 col-md-10 ml-auto">
              <div
                className="owl-carousel owl-theme"
                data-dots="false"
                data-items="3"
                data-md-items="2"
                data-sm-items="1"
                data-autoplay="true"
              ></div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            {data.members.map((logo, index) => {
              return (
                <div className="col-lg-3 col-md-6 sm-mt-3" key={index}>
                  <div className="clients-logo">
                    <img
                      className="img-center"
                      src={`${logo.img_url}`}
                      alt=""
                    />
                    <div className="title">{logo.name}</div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
};
export default Team;
