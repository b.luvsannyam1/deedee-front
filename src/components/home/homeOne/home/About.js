import React from "react";

const About = ({ data }) => {
  return (
    <>
      <section
        className="pos-r bg-contain bg-pos-l py-10"
        data-bg-img="images/bg/02.png"
      >
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-7 col-md-12 image-column p-0 pr-lg-5">
              <div className="img-box box-shadow">
                <div className="box-loader">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
                <img className="img-center" src="images/banner/03.jpg" alt="" />
              </div>
            </div>
            <div className="col-lg-5 ml-auto col-md-12 md-mt-5">
              <div className="section-title mb-4">
                <div className="title-effect">
                  <div className="bar bar-top"></div>
                  <div className="bar bar-right"></div>
                  <div className="bar bar-bottom"></div>
                  <div className="bar bar-left"></div>
                </div>
                <h6>{data.title}</h6>
                <h2>{data.description}</h2>
              </div>
              <ul className="list-unstyled list-icon mb-4">
                {data.details.map((detail, index) => (
                  <li className="mb-3" key={index}>
                    <i className="la la-check"></i>
                    {detail}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default About;
