import React from "react";
import Features from "./Features";
import About from "./About";
import Services from "./Services";
import Steps from "./Steps";
import Team from "./Team";
import Pricing from "./Pricing";
import Testimonial from "./Testimonial";
import Blogs from "./Blogs";
import HeroSection from "./HeroSection";
import Counter from "../../homeThree/content/Counter";
import Questions from "./Questions";

const PageContent = ({ data }) => {
  const { content } = data;
  return (
    <>
      <HeroSection data={content.herosection} />
      <div className="page-content">
        <Features data={content.features} />
        <About data={content.about} />
        <Services data={content.services[0].services} />
        <Steps data={content.steps[0]} />
        <Team data={content.team[0]} />
        <Pricing data={content.pricing} />
        <Questions data={content.faq} />
        <Counter data={content.counter} />
        <Testimonial data={content.testimonial[0]} />
      </div>
    </>
  );
};
export default PageContent;
