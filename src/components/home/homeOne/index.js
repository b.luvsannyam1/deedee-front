import React, { useContext, useEffect } from "react";
import { ContentData } from "../../context";
import PageContent from "./home";

const HomeOne = () => {
  const context = useContext(ContentData);
  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  });
  return (
    <>
      <div className="page-wrapper">
        <PageContent data={context.data} />
      </div>
    </>
  );
};
export default HomeOne;
