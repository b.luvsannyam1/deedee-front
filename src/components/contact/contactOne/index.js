import React, { useEffect } from "react";
import Title from "./Title";
import Form from "./Form";

const ContactOne = () => {
  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  });
  return (
    <>
      <Title />
      <Form />
    </>
  );
};
export default ContactOne;
