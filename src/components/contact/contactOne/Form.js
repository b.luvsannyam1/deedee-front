import React, { useContext } from "react";
import { useForm } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { ContentData } from "../../context";

const Form = () => {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = (data) => {
    alert(JSON.stringify(data));
  };
  // let Locale = "en";
  const { data } = useContext(ContentData);
  return (
    <div className="page-content">
      <section className="contact-1">
        <div className="container">
          <div className="row">
            <div className="col-xl-6 col-lg-8 col-md-12 mr-auto">
              <div className="section-title">
                <div className="title-effect">
                  <div className="bar bar-top"></div>
                  <div className="bar bar-right"></div>
                  <div className="bar bar-bottom"></div>
                  <div className="bar bar-left"></div>
                </div>
                <h6>
                  <FormattedMessage
                    id="contact.subtitle"
                    defaultMessage="Зураг"
                  />
                </h6>
                <h2>
                  <FormattedMessage id="contact.title" defaultMessage="Зураг" />
                </h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <form id="contact-form" onSubmit={handleSubmit(onSubmit)}>
                <div className="messages"></div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="firstName">
                        <FormattedMessage
                          id="contact.firstname"
                          defaultMessage="Зураг"
                        />
                      </label>
                      {errors.firstName && (
                        <span className="help-block with-errors">
                          <FormattedMessage
                            id="contact.lastname"
                            defaultMessage="Зураг"
                          />
                        </span>
                      )}

                      <input
                        id="form_name"
                        type="text"
                        name="firstName"
                        className="form-control"
                        placeholder="Type First name"
                        ref={register({
                          required: "Required",
                        })}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="lastName">
                        {" "}
                        <FormattedMessage
                          id="contact.lastname"
                          defaultMessage="Зураг"
                        />
                      </label>
                      {errors.lastName && (
                        <span className="help-block with-errors">
                          Lastname is required.
                        </span>
                      )}
                      <input
                        id="form_lastname"
                        type="text"
                        name="lastName"
                        className="form-control"
                        placeholder="Type Last name"
                        ref={register({
                          required: "Required",
                        })}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="email">
                        <FormattedMessage
                          id="contact.email"
                          defaultMessage="Зураг"
                        />
                      </label>
                      {errors.email && (
                        <span className="help-block with-errors">
                          Valid email is required.
                        </span>
                      )}
                      <input
                        id="form_email"
                        type="email"
                        name="email"
                        className="form-control"
                        placeholder="Type Email"
                        ref={register({
                          required: "Required",
                          pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                          },
                        })}
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>
                        <FormattedMessage
                          id="contact.phone"
                          defaultMessage="Зураг"
                        />
                      </label>
                      {errors.phone && (
                        <span className="help-block with-errors">
                          Valid phone required (10 digits)
                        </span>
                      )}

                      <input
                        id="form_phone"
                        type="tel"
                        name="phone"
                        className="form-control"
                        placeholder="Type Phone"
                        ref={register({
                          required: "Required",
                          pattern: {
                            value: /^\d{10}$/,
                          },
                        })}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">
                      <label>
                        <FormattedMessage
                          id="contact.message"
                          defaultMessage="Зураг"
                        />
                      </label>
                      {errors.message && (
                        <span className="help-block with-errors">
                          Please,leave us a message.
                        </span>
                      )}
                      <textarea
                        id="form_message"
                        name="message"
                        className="form-control"
                        placeholder="Type Message"
                        rows="4"
                        ref={register({
                          required: "Required",
                        })}
                      ></textarea>
                    </div>
                  </div>
                  <div className="col-md-12 mt-2">
                    <button
                      className="btn btn-theme btn-circle"
                      data-text="Send Message"
                      type="submit"
                    >
                      <span>S</span>
                      <span>e</span>
                      <span>n</span>
                      <span>d</span>
                      <span> </span>
                      <span>M</span>
                      <span>e</span>
                      <span>s</span>
                      <span>s</span>
                      <span>a</span>
                      <span>g</span>
                      <span>e</span>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

      <section className="contact-info p-0 z-index-1">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-4 col-md-12">
              <div className="contact-media">
                {" "}
                <i className="flaticon-paper-plane"></i>
                <span>
                  <FormattedMessage
                    id="contact.address"
                    defaultMessage="Хаяг"
                  />
                </span>
                <p>{data.address}</p>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 md-mt-5">
              <div className="contact-media">
                {" "}
                <i className="flaticon-email"></i>
                <span>
                  <FormattedMessage
                    id="contact.email-address"
                    defaultMessage="Зураг"
                  />
                </span>
                <a href={`mailto:${data.email}`}> {data.email}</a>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 md-mt-5">
              <div className="contact-media">
                {" "}
                <i className="flaticon-social-media"></i>
                <span>
                  <FormattedMessage
                    id="contact.phone-number"
                    defaultMessage="Зураг"
                  />
                </span>
                <a href={`mailto:${data.number}`}>{data.number}</a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="o-hidden p-0 custom-mt-10 z-index-0">
        <div className="container-fluid p-0">
          <div className="row align-items-center">
            <div className="col-md-12">
              <div className="map iframe-h-2">
                <iframe
                  title="GoogleMap"
                  src={data.googleMap}
                  allowfullscreen=""
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Form;
