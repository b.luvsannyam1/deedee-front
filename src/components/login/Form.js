import React, { useContext } from "react";
import { ContentData } from "../context";

const Form = () => {
  const { data } = useContext(ContentData);

  return (
    <div className="page-content">
      <section className="login">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <img className="img-fluid" src="images/banner/06.png" alt="" />
            </div>
            <div className="col-lg-5 col-md-12 ml-auto mr-auto md-mt-5">
              <div className="login-form text-center">
                <img className="img-center mb-5" src={`${data.logo}`} alt="" />
                <div id="contact-form">
                  <div className="messages"></div>
                  <div className="form-group">
                    <input
                      id="form_name"
                      type="text"
                      name="username"
                      className="form-control"
                      placeholder="Email"
                      required="required"
                      data-error="Username is required."
                    />
                  </div>
                  <div className="form-group">
                    <input
                      id="form_password"
                      type="password"
                      name="password"
                      className="form-control"
                      placeholder="Password"
                      required="required"
                      data-error="password is required."
                    />
                  </div>
                  {/* <div className="form-group mt-4 mb-5">
										<div className="remember-checkbox d-flex align-items-center justify-content-between">
											<div className="checkbox">
												<input
													type="checkbox"
													id="check2"
													name="check2"
												/>
												<label for="check2">
													Remember me
												</label>
											</div>
											<a href="/#">Forgot Password?</a>
										</div>
                                    </div>{" "}
                                        */}
                  <button
                    href="/#"
                    className="btn btn-theme btn-block btn-circle"
                    data-text="Sign in"
                    type="submit"
                  >
                    <span>S</span>
                    <span>i</span>
                    <span>g</span>
                    <span>n</span>
                    <span> </span>
                    <span>I</span>
                    <span>n</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Form;
