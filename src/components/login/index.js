import React, { useEffect } from "react";

import Title from "./Title";
import Form from "./Form";

const Login = () => {
  useEffect(() => {
    window.scroll({
      top: 200,
      left: 0,
      behavior: "smooth",
    });
  });

  return (
    <>
      <Title />
      <Form />
    </>
  );
};
export default Login;
