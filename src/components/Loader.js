import React from "react";

const Loader = () => {
  return (
    <div id="ht-preloader">
      <div className="loader clear-loader">
        <div className="loader-box"></div>
        <div className="loader-box"></div>
        <div className="loader-box"></div>
        <div className="loader-box"></div>
        <div className="loader-wrap-text">
          <div className="text">
            <span>D</span>
            <span>E</span>
            <span>E</span>
            <span>D</span>
            <span>E</span>
            <span>E</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Loader;
