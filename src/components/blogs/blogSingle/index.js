import React, { useEffect, useState } from "react";
import Title from "./Title";
import Content from "./Content";
import Loader from "../../Loader";

const Blog = (props) => {
  const [Blog, setBlog] = useState(false);

  useEffect(() => {
    const fetchBlog = () => {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };

      fetch(
        `http://localhost:8080/server/v1/blog/${props.match.params.id}`,
        requestOptions
      )
        .then((response) => response.json())
        .then((result) => setBlog(result.data))
        .catch((error) => console.log("error", error));
    };
    fetchBlog();
  }, [props.match.params.id]);
  return (
    <>
      {Blog ? (
        <div>
          <Title data={Blog} />
          <Content data={Blog} />
        </div>
      ) : (
        <Loader />
      )}
    </>
  );
};
export default Blog;
