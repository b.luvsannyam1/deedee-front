import React, { useContext } from "react";
import { FormattedMessage } from "react-intl";
import { ContentData } from "../../context";

const Title = (props) => {
  const { Locale } = useContext(ContentData);
  return (
    <section
      className="page-title o-hidden pos-r md-text-center"
      data-bg-color="#fbf3ed"
    >
      <canvas id="confetti"></canvas>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-7 col-md-12"></div>
          <div className="col-lg-5 col-md-12 text-lg-right md-mt-3">
            <nav aria-label="breadcrumb" className="page-breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <a href="/">
                    {" "}
                    <FormattedMessage id="title.home" defaultMessage="Зураг" />
                  </a>
                </li>
                <li className="breadcrumb-item">
                  <a href="/blogs">
                    <FormattedMessage id="title.blogs" defaultMessage="Зураг" />
                  </a>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  {props.data.content[Locale].title}
                </li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Title;
