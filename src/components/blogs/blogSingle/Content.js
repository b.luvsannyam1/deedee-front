import React, { useContext, useState, useEffect } from "react";
import { ContentData } from "../../context";
import { FormattedMessage } from "react-intl";

const Content = (props) => {
  const { Locale } = useContext(ContentData);
  const DateFinder = () => {
    let date = new Date(props.data.createdAt);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
      dt = "0" + dt;
    }
    if (month < 10) {
      month = "0" + month;
    }
    let dateString = year + "." + month + "." + dt;
    return dateString;
  };
  const [Latest, setLatest] = useState(false);
  const LatestBlog = () => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch("http://localhost:8080/server/v1/blog/?limit=3", requestOptions)
      .then((response) => response.json())
      .then((result) => setLatest(result.data))
      .catch((error) => console.log("error", error));
  };
  useEffect(() => {
    if (!Latest) LatestBlog();
  });
  return (
    <div className="page-content">
      <section>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 col-md-12">
              <div className="left-side">
                <div className="post">
                  <div className="post-image">
                    <img
                      alt="content-poster"
                      className="img-fluid"
                      src={`${props.data.img_src}`}
                    />{" "}
                    <a className="post-categories" href="/#">
                      <FormattedMessage
                        id="blog.photo"
                        defaultMessage="Зураг"
                      />
                    </a>
                  </div>
                  <div className="post-desc">
                    <div className="post-meta">
                      <ul className="list-inline">
                        <li>
                          <i className="la la-calendar mr-1"></i> <DateFinder />
                        </li>
                      </ul>
                    </div>
                    <div className="post-title">
                      <h2>{props.data.content[Locale].title}</h2>
                    </div>
                    <p className="lead">
                      {props.data.content[Locale].description}
                    </p>
                    <div
                      className="body"
                      dangerouslySetInnerHTML={{
                        __html: props.data.content[Locale].html,
                      }}
                    ></div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-12 sidebar md-mt-5">
              <div className="widget">
                <h5 className="widget-title">Recent Post</h5>
                <div className="recent-post">
                  <ul className="list-unstyled">
                    {Latest !== false
                      ? Latest.map((post, index) => {
                          let date = new Date(post.createdAt);
                          let year = date.getFullYear();
                          let month = date.getMonth() + 1;
                          let dt = date.getDate();

                          if (dt < 10) {
                            dt = "0" + dt;
                          }
                          if (month < 10) {
                            month = "0" + month;
                          }
                          let dateString = year + "." + month + "." + dt;
                          return (
                            <li className="mb-3" key={index}>
                              <div className="recent-post-thumb">
                                <img
                                  className="img-fluid"
                                  src={`${post.img_src}`}
                                  alt=""
                                />
                              </div>
                              <div className="recent-post-desc">
                                <a href={`/blog/${post._id}`}>
                                  {post.content[Locale].title}
                                </a>{" "}
                                <div className="post-date">{dateString}</div>
                              </div>
                            </li>
                          );
                        })
                      : ""}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Content;
