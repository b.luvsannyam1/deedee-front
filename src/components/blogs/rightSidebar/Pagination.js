import React from "react";
import { NavLink } from "react-router-dom";
import "./Pagination.scss";

const PaginationItem = (props) => {
  let className = "";
  props.active
    ? (className = "PaginationItem active")
    : (className = "PaginationItem");
  return (
    <div className={className}>
      <div>{props.text}</div>
    </div>
  );
};

const Pagination = (props) => {
  let paginationButtons = [];
  var i;
  paginationButtons.push(
    1 === props.current ? (
      <PaginationItem text="<<" />
    ) : (
      <NavLink
        activeClassName="PaginationItem active"
        to={`/${props.where}/${props.current - 1}`}
      >
        <PaginationItem text="<<" />
      </NavLink>
    )
  );
  if (props.pageCount <= 6) {
    for (i = 0; i < props.pageCount; i++) {
      i + 1 === props.current
        ? paginationButtons.push(
            <NavLink activeClassName="active" to={`/${props.where}/${i + 1}`}>
              <PaginationItem active={true} text={`${i + 1}`} />
            </NavLink>
          )
        : paginationButtons.push(
            <NavLink activeClassName="active" to={`/${props.where}/${i + 1}`}>
              <PaginationItem text={`${i + 1}`} />
            </NavLink>
          );
    }
  } else {
    if (props.current > 3 && props.pageCount > props.current + 2) {
      paginationButtons.push(
        <NavLink activeClassName="active" to={`/${props.where}/${i + 1}`}>
          <PaginationItem text="1" />
        </NavLink>
      );
      paginationButtons.push(<PaginationItem text="..." />);
      for (i = props.current - 2; i < props.current + 1; i++) {
        i + 1 === props.current
          ? paginationButtons.push(
              <NavLink activeClassName="active" to={`/${props.where}/${i + 1}`}>
                <PaginationItem active={true} text={`${i + 1}`} />
              </NavLink>
            )
          : paginationButtons.push(
              <NavLink activeClassName="active" to={`/${props.where}/${i + 1}`}>
                <PaginationItem text={`${i + 1}`} />
              </NavLink>
            );
      }
      paginationButtons.push(<PaginationItem text="..." />);
      paginationButtons.push(
        <NavLink
          activeClassName="active"
          to={`/${props.where}/${props.pageCount}`}
        >
          <PaginationItem text={props.pageCount} />
        </NavLink>
      );
    } else {
      if (props.current <= 3) {
        for (i = 0; i < 4; i++) {
          i + 1 === props.current
            ? paginationButtons.push(
                <PaginationItem active={true} text={`${i + 1}`} />
              )
            : paginationButtons.push(
                <NavLink
                  activeClassName="active"
                  to={`/${props.where}/${i + 1}`}
                >
                  <PaginationItem text={`${i + 1}`} />
                </NavLink>
              );
        }
        paginationButtons.push(<PaginationItem text="..." />);
        paginationButtons.push(
          <NavLink
            activeClassName="active"
            to={`/${props.where}/${props.pageCount}`}
          >
            <PaginationItem text={props.pageCount} />
          </NavLink>
        );
      } else {
        if (props.pageCount <= props.current + 2) {
          paginationButtons.push(<PaginationItem text="1" />);
          paginationButtons.push(<PaginationItem text="..." />);
          for (i = props.current - 2; i < props.pageCount; i++) {
            i + 1 === props.current
              ? paginationButtons.push(
                  <PaginationItem active={true} text={`${i + 1}`} />
                )
              : paginationButtons.push(<PaginationItem text={`${i + 1}`} />);
          }
        }
      }
    }
  }

  paginationButtons.push(
    props.pageCount === props.current ? (
      <PaginationItem text=">>" />
    ) : (
      <NavLink
        activeClassName="active"
        to={`/${props.where}/${props.current + 1}`}
      >
        <PaginationItem text=">>" />
      </NavLink>
    )
  );
  return <div className="Pagination">{paginationButtons}</div>;
};

PaginationItem.defaultProps = {
  active: false,
};

Pagination.defaultProps = {
  pageCount: 8,
  current: 6,
};

export { Pagination };
