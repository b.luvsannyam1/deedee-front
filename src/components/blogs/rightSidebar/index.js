import React, { useEffect, useState } from "react";
import Content from "./Content";
import Loader from "../../Loader";

const Sidebar = (props) => {
  const [state, setstate] = useState(false);
  const [Loading, setLoading] = useState(false);

  useEffect(() => {
    const Reload = async () => {
      await setLoading(false);
      await window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
      const fetchBlogs = async () => {
        var requestOptions = await {
          method: "GET",
          redirect: "follow",
        };

        await fetch(
          `http://localhost:8080/server/v1/blog/?page=${props.match.params.id}&limit=4`,
          requestOptions
        )
          .then((response) => response.json())
          .then((result) => setstate(result))
          .catch((error) => console.log("error", error));
      };
      await fetchBlogs();
      await setLoading(true);
    };
    Reload();
  }, [props.match.params.id]);
  return (
    <>
      {Loading === false ? (
        <>
          <Loader />
        </>
      ) : (
        <>
          <div style={{ padding: 25 }}></div>
          <Content
            data={state.data}
            pagination={state.pagination}
            match={props.match.params.id}
          />
        </>
      )}
    </>
  );
};
export default Sidebar;
