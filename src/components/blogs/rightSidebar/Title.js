import React from "react";

const Title = () => {
  return (
    <section
      className="page-title o-hidden pos-r md-text-center"
      data-bg-color="#fbf3ed"
    >
      <canvas id="confetti"></canvas>
      <div className="page-title-pattern"></div>
    </section>
  );
};

export default Title;
