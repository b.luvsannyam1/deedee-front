import React from "react";
import { Context } from "./components/context";
import MainNavigation from "./components/MainNavigation";

function App() {
  return (
    <>
      <Context>
        <MainNavigation />
      </Context>
    </>
  );
}

export default App;
